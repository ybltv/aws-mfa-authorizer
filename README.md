# AWS MFA Authorizer

This is a simple script that helps set up MFA authentication for IAM users with MFA (multi-factor authentication) enabled

## Why this exists

This script is used for a very specific use case: when you use IAM users with MFA authentication required. 

The [official AWS guide](https://aws.amazon.com/premiumsupport/knowledge-center/authenticate-mfa-cli/) suggests that you do `aws sts get-session-token` every time you need to re-authenticate using MFA, which is quite inconvenient. You'd have to know your MFA device's serial, enter your TOTP code and copy-paste from json/text/whatever output. This script makes this process more interactive and quite less copy-paste heavy.

## Usage

Suppose, you have a named profile called **myprofile**. We assume, that **myprofile** has an MFA device associated with it. Just call:

```
$ aws_mfa_authorizer -p myprofile
```

It will then ask you to input your TOTP code (the 6-digit code that Google Authenticator or a similar application shows you):

```
Enter TOTP value for MFA device "arn:aws:iam::111111111111:mfa/myuser" (spaces are ignored)
Token:
```

Go ahead and type it in and press *Enter*.

Then, you should see the following message:

```
------------------------------------------------------
Profile: myprofile-mfa
Session token expires at: Mon 10 May 2021 09:00:00 UTC
------------------------------------------------------
```

You're all set. You can now use the new **myprofile-mfa** named profile:

```sh
aws --profile myprofile-mfa ec2 describe-instances
```

When your session expires, just re-run the first command :)

The script will set `output` and `region` for the new profile to the values from the source profile

## Installation

Just copy the script to your PATH. Example:

1. Clone the repo:

   ```git clone https://gitlab.com/ybltv/aws-mfa-authorizer.git```
2. Make the script executable:

   ```chmod +x aws-mfa-authorizer/aws_mfa_authorizer```
3. Symlink the script to `/usr/local/bin`:

   ```ln -s $(realpath aws-mfa-authorizer/aws_mfa_authorizer) /usr/local/bin/aws_mfa_authorizer```

## Requirements

1. **BASH v4** or newer. Might work with **zsh** - I did not test it
2. **awk**
3. **AWS CLI** - tested with both v1 and v2
4. The source profile must be configured for *IAM user*
5. Source profile user must have the following IAM permissions:
   * `sts:GetCallerIdentity`
   * `iam:ListMFADevices`
   * `sts:GetSessionToken`
6. Only tested with Virtual MFA devices. It **will not** work with U2F tokens. It might work with hardware MFA tokens, although I don't have one to test it

The script was tested on **RHEL 8** and **Fedora 33**, but should work on any Linux distribution.

## Command-line parameters

Following command-line parameters are supported (*short form* | *long form* *parameter*):

* `-p | --profile string` - name of the source profile. The profile must be present, otherwise an error will be thrown. If not specified, **default** profile is used
* `-n | --mfa-profile-name string` - name of the new destination profile. If not specified, `${profile}-mfa` is used, i.e. if the source profile was named **myprofile**, the default MFA profile will be called **myprofile-mfa**
* `-s | --serial string` - serial of your MFA device. If not specified, `aws iam list-mfa-devices` is run and the serial number will be retrieved from there. This option should be used if your IAM user doesn't have `iam:ListMFADevices` permission
* `-d | --duration-seconds integer` - temporary session duration, in seconds. Minimum is 900 (15 minutes); maximum is 129600 (36 hours) for IAM users, and 3600 (1 hour) for root user. If omitted, AWS CLI's default value is used (12 hours for IAM users)
* `-t | --token 6-digit-code` - TOTP token (6-digit code from Google Authenticator or similar). If not specified, the script will ask for it interactively
* `-b | --aws-binary path` - path to the `aws` utility. If not specified, the script expects `aws` to be in your `PATH`
* `-h | --help` - print the help message and exit

All parameters are optional

## License

The code is licensed under [Zlib license](https://choosealicense.com/licenses/zlib/ "Zlib license")
